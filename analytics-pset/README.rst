===============================================
Pathology didactics
===============================================

.. contents:: Table of Contents

Overview
========
Series of exercises for learning about the application of statistics to clinical pathology

Setup R environment (optional)
===================
Download repository::

  cd <working root directory>
  git clone git@bitbucket.org:hermands/R_learning.git

Build docker image::

  cd R_learning
  docker build -t rlearning .

Run docker container::

  docker run -d --rm -p 40020:8787 --name Rlearning -v ``pwd``:/home/rlearner rlearning

Optional flag: "--storage-opt size=30G"

Change password for user (rlearner)::

  docker exec -it Rlearning bash
  passwd rlearner
  exit

To access R environment in Rstudio, point browser to ``https://localhost:40020``.

Stats didactic
==============
CP_stats1.Rmd

To run, open rmarkdown file and select ``Knit``.

Database didactic
=================
Database_intro.CP.v2.Rmd

To run, open rmarkdown file and select ``Knit``.

Contact
=======
daniel.herman2@uphs.upenn.edu
