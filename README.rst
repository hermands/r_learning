===============================================
Pathology didactics
===============================================

.. contents:: Table of Contents

Overview
========
Series of exercises for learning about the application of statistics to clinical pathology

Setup R environment (optional)
===================

Install Docker
--------------
1. Navigate to Mac_ or Windows_ instructions.

.. _Mac: https://store.docker.com/editions/community/docker-ce-desktop-mac/plans/docker-ce-desktop-mac-tier?tab=instructions
.. _Windows: https://store.docker.com/editions/community/docker-ce-desktop-windows

2. Click "Get Docker CE XXXX (stable)" to download
3. Double-click to install
4. You should see new icon in taskbar for docker

Setup this repositories environment (Mac/linux)
-----------------------------------
Download repository::

  cd <working root directory>
  git clone git@bitbucket.org:hermands/R_learning.git

Build docker image::

  cd R_learning
  docker build -t rlearning .

Run docker container::

  docker run -d --rm -p 40020:8787 --name Rlearning -v ``pwd``:/home/rlearner rlearning

Optional flag or linux to specify space: "--storage-opt size=30G"

Change password for user (rlearner)::

  docker exec -it Rlearning bash
  passwd rlearner
  exit

To access R environment in Rstudio, point browser to https://localhost:40020.

Stats didactic
==============

Data
----
data/Stats.exercises.2017.v2.xlsx


Analysis
--------
CP_stats1.Rmd

To run, open rmarkdown file and select ``Knit``.

Database didactic
=================
Database_intro.CP.v2.Rmd

To run, open rmarkdown file and select ``Knit``.

Analytics problem set
=====================

Data
----
Training_data_ (data/ajh23643-sup-0006-suppinfo06training_mds_nona.txt)

Test_data_ (data/ajh23643-sup-0005-suppinfo05test_mds_nona.txt)

.. _Training_data: https://bitbucket.org/hermands/r_learning/raw/01303baf7b2d31799c5c61c97f2ce6ef6d08ad2d/data/ajh23643-sup-0006-suppinfo06training_mds_nona.txt
.. _Test_data: https://bitbucket.org/hermands/r_learning/raw/01303baf7b2d31799c5c61c97f2ce6ef6d08ad2d/data/ajh23643-sup-0005-suppinfo05test_mds_nona.txt

Questions
--------
Exercise_ (Path_analytics_exercises.Rst)

.. _Exercise: https://bitbucket.org/hermands/r_learning/src/378c46e17567d71ce5953749ccc8b0107a45e94f/Path_analytics_exercises.Rst?at=analytics-pset&fileviewer=file-view-default

Contact
=======
daniel.herman2@uphs.upenn.edu
