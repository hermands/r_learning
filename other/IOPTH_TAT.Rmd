---
title: "IOPTH TAT"
output: html_notebook
---

```{r setup, echo=FALSE}
suppressMessages(suppressWarnings(library(data.table)))
suppressMessages(suppressWarnings(library(tidyverse)))
suppressMessages(suppressWarnings(library(magrittr)))
# suppressMessages(suppressWarnings(library(zoo)))
# suppressMessages(suppressWarnings(library(knitr)))
# suppressMessages(suppressWarnings(library(logging)))
# suppressMessages(suppressWarnings(library(lubridate)))
# suppressMessages(suppressWarnings(library(kableExtra)))
# suppressMessages(suppressWarnings(library(stringr)))
library(readxl)
```

```{r load_data, echo=FALSE, cache=TRUE}
fpath <- "/data/raw_data/ENDO/IOPTH/Copy of IOPTH times on PCAM samples.DSH.xlsx"
fsheets <- excel_sheets(fpath)
hc_fsheets <- paste("Sheet", 2:9, sep="")

df <- do.call('rbind',
        lapply(hc_fsheets,
       function(x) {
         read_excel(path=fpath, sheet=x)[, 1:12]
       }))

```

```{r process_data, echo=FALSE, results='hide'}

table(df$`Pre/Intra`, exclude = c())
table(df$`Preordered?`, exclude=c())

df %<>%
  filter(`Pre/Intra` != "Pre" & 
           `Preordered?` != "Yes")

df %<>%
  mutate(Coll_Time = as.POSIXct(strptime(x=df$`Collection time`, format="%I:%M%p")),
         Call_time = as.POSIXct(strptime(x=df$`Call time`, format="%I:%M%p")),
         Order_time = as.POSIXct(strptime(x=df$`Order/rec time`, format="%I:%M%p")),
         Ver_time = as.POSIXct(strptime(x=df$`Verification time`, format="%I:%M%p")))

df %<>%
  mutate(Coll2Call = as.numeric(df$Call_time - df$Coll_Time),
         Coll2Order = as.numeric(df$Order_time - df$Coll_Time),
         Order2Call = as.numeric(df$Call_time - df$Order_time))
```
# Collect to Call
```{r analyse_data}

df %>%
  filter(!is.na(Coll2Call)) %>%
  group_by(Location) %>%
  summarize(N=sum(!is.na(Coll2Call)),
            `# of days` = n_distinct(Date),
            `# of pts` = n_distinct(Name),
            `% > 40 min` = format(sum(Coll2Call > 40, na.rm=T) /n() * 100, digits=2),
            `% > 50 min` = format(sum(Coll2Call > 50, na.rm=T) /n() * 100, digits=2),
              mean = mean(Coll2Call, na.rm=T),
            median = median(Coll2Call, na.rm=T))
```

```{r plot_data}

g <- df %>%
  ggplot(aes(x=Coll2Call, color=Location))
g <- g + geom_density()
g <- g + xlab("Time from collection to call (min)")
g

```



# Collect to Order
```{r analyze_data_order}

df %>%
  filter(!is.na(Coll2Order)) %>%
  group_by(Location) %>%
  summarize(N=sum(!is.na(Coll2Order)),
            `# of days` = n_distinct(Date),
            `# of pts` = n_distinct(Name),
            `% > 10 min` = format(sum(Coll2Order > 10, na.rm=T) /n() * 100, digits=2),
            `% > 15 min` = format(sum(Coll2Order > 15, na.rm=T) /n() * 100, digits=2),
              mean = mean(Coll2Order, na.rm=T),
            median = median(Coll2Order, na.rm=T))

```

```{r plot_data_CO}

g <- df %>%
  ggplot(aes(x=Coll2Order, color=Location))
g <- g + geom_density()
g <- g + xlab("Time from collection to call (min)")
g

```


# Order to Call
```{r analyze_data_inLab}

df %>%
  filter(!is.na(Order2Call)) %>%
  group_by(Location) %>%
  summarize(N=sum(!is.na(Order2Call)),
            `# of days` = n_distinct(Date),
            `# of pts` = n_distinct(Name),
            `% > 10 min` = format(sum(Order2Call > 10, na.rm=T) /n() * 100, digits=2),
            `% > 15 min` = format(sum(Order2Call > 15, na.rm=T) /n() * 100, digits=2),
              mean = mean(Order2Call, na.rm=T),
            median = median(Order2Call, na.rm=T))

```

```{r plot_data_iL}

g <- df %>%
  ggplot(aes(x=Order2Call, color=Location))
g <- g + geom_density()
g <- g + xlab("Time from collection to call (min)")
g

```


# Outliers
```{r analyze_data_2}

df %>%
  filter(Coll2Call > 50) %>% print.data.frame
```


